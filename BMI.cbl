       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL5.
       AUTHOR. NARACHAI.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  HEIGHT    PIC 9(3).
       01  WEIGTH    PIC 9(3).
       01  BMI       PIC 99v9.
       PROCEDURE DIVISION.
       BEGIN. 
           DISPLAY "Enter Height (cm)- " WITH NO ADVANCING 
           ACCEPT HEIGHT 
           DISPLAY "Enter Weigth (kg)- " WITH NO ADVANCING 
           ACCEPT WEIGTH 
           COMPUTE BMI = WEIGTH / ((HEIGHT/100)**2)
           DISPLAY "BMI = "BMI
           EVALUATE TRUE 
              WHEN BMI < 16                   DISPLAY "Severe Thinness"
              WHEN BMI >= 16.0 AND BMI < 17.0 DISPLAY "Moderate Thinness
      -        ""
              WHEN BMI >= 17.0 AND BMI < 18.5 DISPLAY "Mild Thinness"
              WHEN BMI >= 18.5 AND BMI < 25.0 DISPLAY "Normal"
              WHEN BMI >= 25.0 AND BMI < 30.0 DISPLAY "Overweight"
              WHEN BMI >= 30.0 AND BMI < 35.0 DISPLAY "Obese Class I"
              WHEN BMI >= 35.0 AND BMI < 40.0 DISPLAY "Obese Class II"
              WHEN BMI >= 40.0                DISPLAY "Obese Class III"
           END-EVALUATE 
           GOBACK 
           .
           
       